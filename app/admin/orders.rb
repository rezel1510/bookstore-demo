ActiveAdmin.register Order do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
# actions :index
permit_params :status

  index do
    id_column
    column "Total price", :subtotal
    column "Num. of Product", :total
    column "Customer name", :name
    column :address
    column :status
    actions
  end

  show do
    attributes_table do
      row "Total price", &:subtotal
      row "Num. of Product", &:total
      row "Customer name", &:name
      row :address
      row :status
      panel 'Products' do
        paginated_collection(order.order_items.paginate(page: params[:page]).limit(10), download_links: false) do
          table_for collection do
            column 'ID', :product_id
            column 'Unit price', :unit_price
            column 'Quantity', :quantity
            column 'Total price', :total_price
            column '', class: 'col col-actions' do |order_item|
              div class: 'table_actions' do
               span link_to 'View',"/admin/products/#{order_item.product_id}" , class: 'view_link member_link'
              end
            end
          end
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :subtotal, heading: 'Total price', input_html: { disabled: true } 
      f.input :total, heading: 'Num. of Product', input_html: { disabled: true }  
      f.input :name, heading: 'Customer name', input_html: { disabled: true } 
      f.input :address, input_html: { disabled: true } 
      f.input :status, as: :select, collection: ['PENDING', 'FINISHED'],
              include_blank: false
      f.actions
    end
  end
end
