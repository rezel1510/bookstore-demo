class PostsController < ApplicationController
  require "ostruct"
  require 'httparty'

  def index
    # @posts ||= []
    @posts = Post.all
    # debugger
    respond_to do |format|
      format.html
      format.json { render json: response.body }
    end
  end

  def show
    @post = Post.show(params[:id])
  end

  def create
    # Post.new(1,1, 'abc', 'xyz').to_json
    # response = HTTParty.post('https://jsonplaceholder.typicode.com/posts', form: params)
    puts response.code
  end

end