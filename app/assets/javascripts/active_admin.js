// #= require active_admin/base
$(document).ready(function(){

    var x = document.getElementById('product_img');
    $(x).on('change',function(){
        updatePic(this);
    }); 

    function updatePic(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
              $('#preimg')
                .attr('src', e.target.result)
                .width(300)
                .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
});