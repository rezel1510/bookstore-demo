class Post
  # self.site = "https://jsonplaceholder.typicode.com/posts"
  include ActiveModel::Serializers::JSON
  # require 'CallApiService'
  include ActiveModel::AttributeMethods
  require 'ostruct'

  attr_accessor :userid, :id, :title, :body

  def attributes=(hash)
    hash.each do |key, value|
      send("#{key.downcase}=", value)
    end
  end

  # def attributes
  #   {userid: nil,
  #   id: nil,
  #   title: nil,
  #   body: nil
  #   }
  # end

  def self.call_with_cache(param)
    Cache.fetch "call_with_cache(#{param}" do
      response = CallApiService.init('https://jsonplaceholder.typicode.com/', 'posts').send_get(param)
      p = Post.new
      p.from_json(response.body)
    end
  end

  def self.all
    response = CallApiService.init('https://jsonplaceholder.typicode.com/', 'posts').send_get
    resource = JSON.parse(response.body)
    posts ||= []
    resource.each do |post|
      p = Post.new
      puts p
      posts << p.from_json(post.to_json)
    end
    posts
  end

  def self.show(param)
    response = CallApiService.init('https://jsonplaceholder.typicode.com/', 'posts').send_get(param)
    puts response.body
    p = Post.new
    p.from_json(response.body)
  end
end