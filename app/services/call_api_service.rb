class CallApiService
  include HTTParty
  # CallApiService.new('https://jsonplaceholder.typicode.com', '/posts')
  base_uri 'https://jsonplaceholder.typicode.com/'
  def self.init(base, service)
    # @options = { query: { site: service, page: page } }
    @uri = base
    @service = service
    self.base_uri @uri
    self
  end

  def self.send_get(param='', options='')
    response = HTTParty.get(@uri + @service.to_s + '/' + param.to_s + options)
    # JSON[response]
  end

  def self.send_post(param='')
    response = HTTParty.post(@uri + @service.to_s + '/' + param.to_s)
  end

end