ActiveAdmin.register Product do
  # belongs_to :category
  permit_params :name, :price, :author, :price, :description, :cover, :img, category_ids: []

  form do |f|
    f.inputs do
      f.input :name
      f.input :price
      f.input :author
      f.input :description
      f.input :cover
      f.input :categories, as: :check_boxes
      f.div '', id: 'filepreview'
      f.input :img, as: :file
      panel 'Preview image' do
        if f.object.img.attached?
          image_tag url_for(f.object.img), size: '300x200', id: 'preimg'
        else
          image_tag '', size: '300x200', id: 'preimg'
        end
      end
      f.actions
    end
  end

  show do |s|
    attributes_table do
      row :image do |ad|
        if s.img.attached?
          image_tag url_for(ad.img), size: '300x200'
        end
      end
      row :name
      row :price
      row :author
      row :description
      row :cover
      row :rating
      row :times_rated
      panel 'Categories' do
        paginated_collection(product.categories.paginate(page: params[:page]).limit(10), download_links: false) do
          table_for collection do
            column :name
          end
        end
      end
    end
  end

end
