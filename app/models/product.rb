class Product < ApplicationRecord
  has_one_attached :img
  has_many :taggings
  has_many :categories, through: :taggings
  accepts_nested_attributes_for :categories, :allow_destroy => true
  validates :slug,  presence: true, uniqueness: true

  permitted = [:name, :price, :author, :price, :description, :cover,
    category_attributes: [:id]]

  has_many :order_items
  has_many :comments
  before_save :set_default
  before_validation :create_slug
  def rate(rating)
    self[:total_rating] += rating
    self[:times_rated] += 1
    set_rating
  end

  def to_param
    "#{id}-#{slug}"
  end
  
 
  private
  
  def create_slug
    self.slug = name.try(:parameterize) if slug.blank?
  end

  def set_rating
    self[:rating] = self[:total_rating] / self[:times_rated]
  end

  def set_default
    if self[:rating].nil? && self[:times_rated].nil? && self[:total_rating].nil?
      self[:rating] = 0
      self[:times_rated] = 0
      self[:total_rating] = 0
    elsif self[:times_rated].positive?
      set_rating
    end
  end
end
