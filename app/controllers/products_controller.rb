class ProductsController < ApplicationController
  add_breadcrumb "Product", :products_path

  def index
    @cart = session[:cart]
    @cart ||= []
    # bob = Post.first
    # jon = Jon.first
    # debugger
    # puts bob
    @categories = Category.all
    @products = Product.all
    @order_item = OrderItem.new
  end

  def show
    @category = Category.all
    @product = Product.find(params[:id])
    @order_item = OrderItem.new
    @comments = Comment.all
    @comment = Comment.new
    # # debugger
    # render :json => {
    #   :id => @product.id,
    #   :name => @product.name
    # }
    # render action: :show
    add_breadcrumb @product, product_path(@product)
    respond_to do |format|
      format.html
      format.json { render json: @product }
    end
  end
  #
  # def rate
  #   product = Product.find(params[:id])
  #   product.rate(params[:rating])
  #   product.save
  # end
end
