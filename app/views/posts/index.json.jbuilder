json.array!(@posts) do |post|
    json.extract! post, :userId, :id, :title, :body
end